﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using CommonEngine;
public class UnityAds : IAdsProvider {

    System.Action<AdResult> adCallback;

    public void ShowRewaredAd(System.Action<AdResult> callback)
    {
        adCallback = callback;
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show("rewardedVideo", options);
    }

    void HandleShowResult(ShowResult result)
    {
        if(result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
            adCallback(AdResult.Finished);
        }
        else if(result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            adCallback(AdResult.Skipped);

        }
        else if(result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            adCallback(AdResult.Failed);
        }

        adCallback = null;
    }

    
}
