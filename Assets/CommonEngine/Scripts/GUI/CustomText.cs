﻿using UnityEngine;
using UnityEngine.UI;

namespace CommonEngine
{
    public enum CustomTextMode
    {
        ToUpper,
        ToLower,
        FirstCapital,
        EveryFirstCapital
    }
    [RequireComponent (typeof(Text))]
    public class CustomText : MonoBehaviour
    {
        public string key;
        public CustomTextMode mode;

        void Start()
        {
            SetText(ModelManager.Instance.Translations.GetTextByKey(key));
        }

        void SetText(string text)
        {
            switch(mode)
            {
                case CustomTextMode.FirstCapital:
                    {
                        char[] chars = text.ToCharArray();
                        chars[0] = char.ToUpper(chars[0]);
                        text = new string(chars);
                        break;
                    }
                case CustomTextMode.EveryFirstCapital:
                    {
                        var words = text.Split(' ');
                        char[] chars;
                        text = string.Empty;
                        foreach(var w in words)
                        {
                            chars = w.ToCharArray();
                            chars[0] = char.ToUpper(chars[0]);
                            text += new string(chars);
                        }
                        break;
                    }
                case CustomTextMode.ToLower: text = text.ToLower(); break;
                case CustomTextMode.ToUpper: text = text.ToUpper(); break;
            }
            GetComponent<Text>().text = text;
        }
    }
}