﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine {

    public enum AdResult
    {
        Finished,
        Skipped,
        Failed
    }

    public class AdsManager : Utils.MonoBehaviourSingleton<AdsManager> {

        UnityAds unityAds;

        void Awake()
        {
            unityAds = new UnityAds();
        }

        public void ShowRewardedAd(System.Action<AdResult> callback)
        {
            unityAds.ShowRewaredAd(callback);
        }
    }

}