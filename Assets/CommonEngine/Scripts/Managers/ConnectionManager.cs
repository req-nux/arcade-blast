﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine
{
    public class ConnectionManager : Utils.MonoBehaviourSingleton<ConnectionManager>
    {

        public bool HasInternetConnection()
        {
            return Application.internetReachability != NetworkReachability.NotReachable;
        }
    }
}