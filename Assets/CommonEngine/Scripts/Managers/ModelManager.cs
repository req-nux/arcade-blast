﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEngine.Model;

namespace CommonEngine
{
    public class ModelManager : CommonEngine.Utils.MonoBehaviourSingleton<ModelManager>
    {
        public string settingsURL;
        public string translationsURL;
        public Settings Settings { get; private set; }
        public Translations Translations { get; private set; }

        SystemLanguage Language;
        SystemLanguage defaultLanguage = SystemLanguage.English;

        public void Init()
        {
            if(ConnectionManager.Instance.HasInternetConnection())
                GetComponent<ModelDownloader>().DownloadTranslations(OnTranslationsDownloaded);
            else
                GetComponent<ModelDownloader>().LoadLocalTranslations(OnLoadLocalTranslations);
        }

        void OnTranslationsDownloaded(string sheetText)
        {
            SetTranslations(sheetText);

            if(StateManager.Instance.GetActiveState() is LoadingState)
                StateManager.Instance.GetState<LoadingState>().OnModelDownloaded();
        }

        void OnLoadLocalTranslations(string sheetText)
        {
            SetTranslations(sheetText, SheetExtension.TSV);

            if(StateManager.Instance.GetActiveState() is LoadingState)
                StateManager.Instance.GetState<LoadingState>().OnModelDownloaded();
        }

        void SetTranslations(string sheetText, SheetExtension ext = SheetExtension.TSV)
        {
            Language = SheetParser.ColumnExists(sheetText, Application.systemLanguage.ToString())
                        ? Application.systemLanguage : defaultLanguage;
            Translations = new Translations(SheetParser.SheetToDictionary(sheetText, Language.ToString(), ext));
        }
    }
}