﻿using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine
{
    public class StateManager : Utils.MonoBehaviourSingleton<StateManager>
    {
        State[] states;
        Stack<State> stateStack;

        public void Init()
        {
            states = GetComponentsInChildren<State>();
            stateStack = new Stack<State>();

            PushState<LoadingState>(null);
        }

        public void PushState<T>(StateInitParams initParams) where T : State
        {
            var state = GetState<T>();
            if(stateStack.Count > 0)
                stateStack.Peek().Freeze();
            stateStack.Push(state);
            state.Init(initParams);
        }

        public void PopState()
        {
            var state = stateStack.Pop();
            state.Finish();
            if(stateStack.Count > 0)
                stateStack.Peek().Unfreeze();
        }
        public void PopStateUntil<T>()
        {
            while(stateStack.Count > 1 && !(stateStack.Peek() is T))
                PopState();
        }

        public T GetState<T>() where T : State
        {
            foreach(var state in states)
            {
                if(state is T)
                    return (T)state;
            }
            Debug.LogErrorFormat("Can't find state of type {0}. Check if it exists in MainScene.", typeof(T).Name);
            return null;
        }

        public State GetActiveState()
        {
            return stateStack.Peek();
        }
    }
}