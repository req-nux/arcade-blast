﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine
{
    public class TimeManager : CommonEngine.Utils.Singleton<TimeManager>
    {
        public float Time
        {
            get { return UnityEngine.Time.time; }
        }

        public float TimeSinceLevelLoad
        {
            get { return UnityEngine.Time.timeSinceLevelLoad; }
        }

        public float TimeScale
        {
            get { return UnityEngine.Time.timeScale; }
        }

    }
}