﻿using System;
using System.Collections;
using UnityEngine;

namespace CommonEngine.Model
{
    public class ModelDownloader : MonoBehaviour
    {
        [SerializeField] float maxDownloadTime = 7f;

        public void DownloadSettings(Action<string> onDownload)
        {
            StartCoroutine(DownloadSettingsCoroutine(onDownload));
        }
        public void DownloadTranslations(Action<string> onDownload)
        {
            StartCoroutine(DownloadTranslationsCoroutine(onDownload));
        }

        public void LoadLocalTranslations(Action<string> onLoad)
        {
            var translations = Resources.Load("Translations") as TextAsset;
            onLoad(translations.text);
        }

        IEnumerator DownloadSettingsCoroutine(Action<string> onDownload)
        {
            yield return null;
        }

        IEnumerator DownloadTranslationsCoroutine(Action<string> onDownload)
        {
            var url = ModelManager.Instance.translationsURL;
            Debug.Log(url);
            var www = new WWW(url);
            yield return www;

            if(!string.IsNullOrEmpty(www.error))
            {
                Debug.Log("Translations downloaded successfully!");
            }
            else
            {
                Debug.LogWarning("Error while downloading translations: " + www.error);
            }
            onDownload(www.text);
        }

    }
}