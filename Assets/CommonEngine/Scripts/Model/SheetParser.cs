﻿using System.Collections.Generic;
using System.IO;

namespace CommonEngine.Model
{
    public enum SheetExtension
    {
        TSV,
        CSV
    }

    public class SheetParser
    {

        public static Dictionary<string, string> SheetToDictionary(string sheetText, string columnName, SheetExtension ext = SheetExtension.TSV)
        {
            var dictionary = new Dictionary<string, string>();
            StringReader reader = new System.IO.StringReader(sheetText);
            char splitToken = GetSplitToken(ext);

            string firstLine = reader.ReadLine();
            if(!firstLine.Contains(columnName))
                throw new System.Exception("SheetParser.SheetToDictionary() : First line does not contains columnName");

            int columnIndex = GetColumnIndex(firstLine, columnName, splitToken);

            string line;
            while((line = reader.ReadLine()) != null)
            {
                var lineTokens = line.Split(splitToken);
                dictionary.Add(lineTokens[0], lineTokens[columnIndex]);
            }

            return dictionary;
        }

        public static bool ColumnExists(string sheetText, string columnName)
        {
            StringReader reader = new StringReader(sheetText);
            return reader.ReadLine().Contains(columnName);
        }

        static int GetColumnIndex(string line, string columnName, char splitToken)
        {
            var names = line.Split(splitToken);
            for(int i = 0; i < names.Length; i++)
            {
                if(names[i].Equals(columnName))
                    return i;
            }
            throw new System.Exception("SheetParser.GetColumnIndex() : Line does not contains columnName");
        }

        static char GetSplitToken(SheetExtension ext)
        {
            switch(ext)
            {
                case SheetExtension.TSV: return '\t';
                case SheetExtension.CSV:
                default: return ',';
            }
        }
        /*
        public static List<string> GetKeys(string sheetText)
        {
            List<string> keys = new List<string>();
            using(System.IO.StringReader reader = new System.IO.StringReader(sheetText))
            {
                string line = reader.ReadLine(); // read and ignore first line
                while((line = reader.ReadLine()) != null)
                {
                    var firstWord = line.IndexOf(splitToken) > -1 ? line.Substring(0, line.IndexOf(splitToken)) : line;
                    keys.Add(firstWord);
                }
            }
            return keys;
        }

        public List<string> GetColumn(string sheetText, string columnName)
        {
            List<string> column = new List<string>();
            using(System.IO.StringReader reader = new System.IO.StringReader(sheetText))
            {
                string firstLine = reader.ReadLine();
                if(!firstLine.Contains(columnName))
                    return null;

                int columnIndex = GetColumnIndex(firstLine,columnName);

                string line;
                while((line = reader.ReadLine()) != null)
                {
                    column.Add(line.Split(splitToken)[columnIndex + 1]);
                }
            }
            return column;
        }
        */

    }
}