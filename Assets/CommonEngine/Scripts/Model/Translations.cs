﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine.Model
{
    public class Translations
    {
        const string errorText = "UNDEFINED TEXT";
        Dictionary<string, string> translations;

        public Translations(Dictionary<string, string> translations)
        {
            this.translations = translations;
        }

        public string GetTextByKey(string key)
        {
            return translations.ContainsKey(key) ? translations[key] : errorText;
        }

    }
}