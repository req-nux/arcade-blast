﻿using CommonEngine;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

    [SerializeField] string loadSceneName = "MainScene";

    AsyncOperation loadOperation;

    void Start () {
        loadOperation = SceneManager.LoadSceneAsync(loadSceneName, LoadSceneMode.Additive);
    }

    void Update()
    {
        if(loadOperation != null && loadOperation.isDone)
            onMainSceneLoaded();
    }

    void onMainSceneLoaded()
    {
        StateManager.Instance.Init();
        ModelManager.Instance.Init();
        enabled = false;
        SceneManager.UnloadSceneAsync("Start");
    }
}
