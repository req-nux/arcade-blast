﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine
{
    public class LoadingState : State
    {

        public override void Init(StateInitParams initParams)
        {
            CreateView<LoadingStateView>();
        }

        public void OnModelDownloaded()
        {
            StateManager.Instance.PopState();
            StateManager.Instance.PushState<StartScreenState>(null);
        }
    }
}