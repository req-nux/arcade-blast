﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NoConnectionState : State
{
    public override void Init(StateInitParams initParams)
    {
        CreateView<NoConnectionStateView>();

        ((NoConnectionStateView)view).Init(OnOk);
    }

    void OnOk()
    {
        StateManager.Instance.PopState();
    }

}
