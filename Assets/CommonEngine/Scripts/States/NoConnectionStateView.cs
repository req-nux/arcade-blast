﻿using CommonEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoConnectionStateView : StateView {

    public Button okButton;

    public void Init(Action onOk)
    {
        okButton.onClick.AddListener(() => onOk());
    }
}
