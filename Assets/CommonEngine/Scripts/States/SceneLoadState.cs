﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

namespace CommonEngine
{
    public class SceneLoadInitParams : StateInitParams
    {
        public string sceneName;
        public Action onSceneLoad;

        public SceneLoadInitParams(string sceneName, Action onSceneLoad)
        {
            this.sceneName = sceneName;
            this.onSceneLoad = onSceneLoad;
        }
    }

    public class SceneLoadState : State
    {
        public override void Init(StateInitParams initParams)
        {
            CreateView<SceneLoadStateView>();

            var sceneName = ((SceneLoadInitParams)initParams).sceneName;
            var onSceneLoad = ((SceneLoadInitParams)initParams).onSceneLoad;
            
            StartCoroutine(LoadCoroutine(sceneName, onSceneLoad));
        }

        System.Collections.IEnumerator LoadCoroutine(string sceneName, Action onSceneLoad)
        {
            AsyncOperation loadOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            while(!loadOperation.isDone)
            {
                yield return null;
            }
            StateManager.Instance.PopState();
            onSceneLoad();
        }
    }
}