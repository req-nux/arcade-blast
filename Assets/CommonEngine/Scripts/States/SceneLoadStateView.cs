﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SceneLoadStateView : StateView {

    public Image loadingScreen;
    public string loadingText;
}
