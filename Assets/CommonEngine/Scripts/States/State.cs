﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace CommonEngine
{
    public abstract class StateInitParams
    {

    }

    public abstract class State : MonoBehaviour
    {
        [SerializeField] protected StateView viewPrefab;
        protected StateView view;

        public abstract void Init(StateInitParams initParams);

        public virtual void CreateView<T>() where T : StateView
        {
            view = (T)Instantiate(viewPrefab);
            PinViewToCanvas(view);            
        }

        public virtual void UpdateState() { }

        public virtual void Freeze()
        {
            view.gameObject.SetActive(false);
        }

        public virtual void Unfreeze()
        {
            view.gameObject.SetActive(true);
        }

        public virtual void Finish()
        {
            Destroy(view.gameObject);
        }

        protected void PinViewToCanvas(StateView view)
        {
            view.transform.SetParent(GameObject.Find("Canvas").transform, false);
        }

    }
}