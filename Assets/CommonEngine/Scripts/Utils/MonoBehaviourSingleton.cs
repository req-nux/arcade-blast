﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEngine.Utils
{

    public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour 
    {

        static T instance = null;

        public static T Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if(FindObjectsOfType(typeof(T)).Length > 1)
                    {
                        Debug.LogError("[Singleton] Something went really wrong " +
                            " - there should never be more than 1 singleton!" +
                            " Reopening the scene might fix it.");
                        return instance;
                    }

                    if(instance == null)
                    {
                        Debug.LogErrorFormat("There is no {0} MonoBehaviourSingleton in the scene!", typeof(T).Name);
                    }
                }
                return instance;
            }
        }

    }
}