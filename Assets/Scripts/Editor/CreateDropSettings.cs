﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class CreateDropSettings
{

    [MenuItem("ArcadeBlast/Create DropSettings")]
    public static void CreateNewCarConfig()
    {
        var config = ScriptableObject.CreateInstance<DropSettings>();

        AssetDatabase.CreateAsset(config, "Assets/NewSettings.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = config;
        Debug.Log("Created DropSettings");

    }
}
