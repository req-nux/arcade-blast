﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSession {

    public int score;
    public int health;
    public bool wasGameOverCalled;
    public float startBallSpeedIncrease;

    public GameSession(int startHealth)
    {
        score = 0;
        health = startHealth;
        wasGameOverCalled = false;
        startBallSpeedIncrease = 0;
    }

}
