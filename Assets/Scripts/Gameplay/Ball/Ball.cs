﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public float speed;
    public float speedGainOnHit = 0.025f;
    public float speedGainOnLevel = 0.3f;
    public bool attached = true;
    public float minSpeed;
    [HideInInspector]
    public Rigidbody2D body;

    Platform platform;

    void Awake()
    {
        platform = GameObject.FindObjectOfType<Platform>();
        body = GetComponent<Rigidbody2D>();
        
    }

    void FixedUpdate()
    {
        if(!attached)
        {
            if(Mathf.Abs(body.velocity.y) < minSpeed)
            {
                var dir = transform.position.y > 0 ? -1 : 1;
                body.velocity = new Vector2(body.velocity.x, minSpeed * dir);
            }
        }
        else
        {
            body.velocity = Vector2.zero;
        }

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        speed += speedGainOnHit;
        if(col.gameObject.GetComponent<Platform>() != null)
        {
            Vector2 dir = transform.position - col.transform.position;
            //dir.Set(dir.x, Mathf.Abs(dir.y));
            body.velocity = dir.normalized * speed;
            foreach(var ballEffect in GetComponents<BallEffect>())
            {
                ballEffect.OnPlatformHit();
            }
        }
        else 
        {
            body.velocity = body.velocity.normalized * speed;
            if(col.gameObject.GetComponent<Block>() != null)
            {
                foreach(var ballEffect in GetComponents<BallEffect>())
                    ballEffect.OnBlockHit(col.gameObject);
            }
            else
            {
                foreach(var ballEffect in GetComponents<BallEffect>())
                    ballEffect.OnWallHit();
            }
        }
    }

}
