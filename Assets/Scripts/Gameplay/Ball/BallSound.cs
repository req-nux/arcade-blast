﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSound : MonoBehaviour {

    [SerializeField] AudioClip blockHit;
    [SerializeField] AudioClip wallHit;


    AudioSource audioSource;

    int blockHitCounter = 0;

    [SerializeField] float maxPitch;
    [SerializeField] int hitsToMaxPitch;
    [SerializeField] float startPitch;
    float pitchGainPerHit;

    void Awake () {
        audioSource = GetComponent<AudioSource>();
        pitchGainPerHit = (maxPitch - startPitch) / hitsToMaxPitch;
    }
    

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Block>() != null)
        {
            audioSource.pitch = startPitch + pitchGainPerHit * blockHitCounter;
            audioSource.PlayOneShot(blockHit);
            blockHitCounter = Mathf.Min(blockHitCounter + 1, hitsToMaxPitch);
        }
        else if(collision.gameObject.GetComponent<Platform>() != null)
        {
            if(!GetComponent<Ball>().attached)
            {
                blockHitCounter = 0;
                audioSource.pitch = 5;
                audioSource.PlayOneShot(wallHit);
            }
            
        }
        else
        {
            audioSource.pitch = 2.5f;
            audioSource.PlayOneShot(wallHit);
        }
    }

}
