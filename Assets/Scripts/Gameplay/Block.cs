﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

    [SerializeField] DropSettings dropSettings;

    bool destroyCalled = false;

    void OnCollisionEnter2D(Collision2D collision)
    {
        DestroyBlock();
    }

    void TryDropBonus()
    {
        if(Random.value < dropSettings.bonusDropChance)
        {
            var bonus = dropSettings.GetItem();
            if(bonus != null)
            {
                var bonusInstance = Instantiate(bonus);
                bonusInstance.transform.position = transform.position;
            }
        }
    }

    public void DestroyBlock()
    {
        if(destroyCalled == true)
            return;

        destroyCalled = true;
        CommonEngine.StateManager.Instance.GetState<GameplayState>().OnBlockDestroy();
        TryDropBonus();
        Destroy(gameObject);
    }
}
