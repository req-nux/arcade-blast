﻿using UnityEngine;

public abstract class BallEffect : MonoBehaviour {

    public abstract void OnPlatformHit();

    public abstract void OnBlockHit(GameObject block);

    public abstract void OnWallHit();
}
