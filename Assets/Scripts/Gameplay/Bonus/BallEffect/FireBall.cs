﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : BallEffect
{
    Collider2D[] blocksInRange;
    float effectRange;

    List<GameObject> effectPool;
    GameObject firePrefab;

    void Awake()
    {
        blocksInRange = new Collider2D[20];
        effectRange = GameObject.FindObjectOfType<LevelGenerator>().blockPositionShift.x * 1.1f;
        firePrefab = Resources.Load("Effects/FlameBlock") as GameObject;
        AttachEffects();

        effectPool = new List<GameObject>();
        for(int i = 0; i < 5; i++)
        {
            var effect = Instantiate(firePrefab);
            effectPool.Add(effect);
        }
    }
    public override void OnBlockHit(GameObject block)
    {
        var target = FindBlockToHit(block);
        if(target != null)
        {
            var effect = GetBlockEffect();
            effect.transform.position = target.transform.position;
            effect.SetActive(true);
            target.GetComponent<Block>().DestroyBlock();
        }
        System.Array.Clear(blocksInRange, 0, blocksInRange.Length);
    }

    public override void OnPlatformHit()
    {
    }

    public override void OnWallHit()
    {
    }

    GameObject FindBlockToHit(GameObject alreadyHitBlock)
    {
        int blocksCount = Physics2D.OverlapCircleNonAlloc(transform.position, effectRange, blocksInRange);
        GameObject target = null;
        if(blocksCount > 0)
        {
            for(int i = 0; i < blocksInRange.Length; i++)
            {
                if(blocksInRange[i] != null && blocksInRange[i].GetComponent<Block>() != null
                    && blocksInRange[i].gameObject != alreadyHitBlock)
                {
                    if(target == null ||
                        (transform.position - target.transform.position).magnitude >
                        (transform.position - blocksInRange[i].transform.position).magnitude)
                    {
                        target = blocksInRange[i].gameObject;
                    }
                }
            }
        }
        return target;
    }

    void AttachEffects()
    {
        GameObject fireObject = Resources.Load("Effects/Fire") as GameObject;
        Instantiate(fireObject, transform, false);
        transform.GetComponentInChildren<SpriteRenderer>().color = new Color(1f, 0.93f, 0.28f);
    }

    GameObject GetBlockEffect()
    {
        for(int i = 0; i < effectPool.Count; i++)
        { 
            if(!effectPool[i].gameObject.activeInHierarchy)
                return effectPool[i];
        }
        var newEffect = Instantiate(firePrefab);
        effectPool.Add(newEffect);
        return newEffect;
    }
}
