﻿using System;
using System.Collections;
using UnityEngine;

public class ThunderBall : BallEffect {

    Collider2D[] blocksHit;
    float thunderRange = 1f;
    float thunderChance = 0.3f;

    ParticleSystem thunder;

    void Awake()
    {
        blocksHit = new Collider2D[20];
        AttachEffects();
    }
    public override void OnBlockHit(GameObject block)
    {
        if(UnityEngine.Random.value > thunderChance)
            return;

        var target = FindBlockToHit(block);
        if(target != null)
        {
            StartCoroutine(PlayThunderEffect(target.transform.position));
            target.GetComponent<Block>().DestroyBlock();
        }
        System.Array.Clear(blocksHit, 0, blocksHit.Length);
    }

    public override void OnPlatformHit()
    {
    }

    public override void OnWallHit()
    {
    }

    GameObject FindBlockToHit(GameObject alreadyHitBlock)
    {
        int blocksCount = Physics2D.OverlapCircleNonAlloc(transform.position, thunderRange, blocksHit);
        if(blocksCount > 0)
        {
            for(int i = 0; i < blocksHit.Length; i++)
            {
                if(blocksHit[i] != null && blocksHit[i].GetComponent<Block>() != null
                    && blocksHit[i].gameObject != alreadyHitBlock)
                {
                    return blocksHit[i].gameObject;
                }
            }
        }
        return null;
    }

    void AimThunder(Vector3 target)
    {
        var thunderTransform = thunder.transform;
        thunderTransform.right = target;
    }

    void AttachEffects()
    {
        GameObject thunderObject = Resources.Load("Effects/Thunder") as GameObject;
        var instance = Instantiate(thunderObject, transform, false);
        thunder = instance.GetComponent<ParticleSystem>();
    }

    IEnumerator PlayThunderEffect(Vector3 target)
    {
        thunder.Play();
        while(thunder.isEmitting)
        {
            AimThunder(target);
            yield return null;
        }
    }

}
