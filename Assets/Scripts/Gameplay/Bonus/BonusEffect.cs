﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BonusEffect : MonoBehaviour {

    public abstract void OnPickup();

    protected void DestroyBonus()
    {
        foreach(var child in transform.GetComponentsInChildren<Transform>())
        {
            if(child != transform)
                child.gameObject.SetActive(false);
        }
        GetComponent<Collider2D>().enabled = false;
        var audio = GetComponent<AudioSource>();
        audio.PlayOneShot(audio.clip);
        Destroy(gameObject, audio.clip.length);
    }

}
