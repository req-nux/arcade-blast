﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpeed : BonusEffect
{
    [SerializeField] float speedGain = 1.5f;
    public override void OnPickup()
    {
        foreach(var ball in GameObject.FindObjectsOfType<Ball>())
        {
            ball.speed = Mathf.Max(2f, ball.speed + speedGain);
            ball.body.velocity = ball.body.velocity.normalized * ball.speed;
        }
        DestroyBonus();
    }
}
