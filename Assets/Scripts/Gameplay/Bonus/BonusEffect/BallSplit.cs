﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSplit : BonusEffect
{
    public override void OnPickup()
    {
        foreach(var ball in GameObject.FindObjectsOfType<Ball>())
        {
            var newBall = Instantiate(ball);
            var velX = ball.body.velocity.x * -1;
            if(velX < 0.1f)
                velX += 0.5f;
            newBall.body.velocity = new Vector2(velX, Mathf.Abs(ball.body.velocity.y));
        }
        DestroyBonus();
    }
}
