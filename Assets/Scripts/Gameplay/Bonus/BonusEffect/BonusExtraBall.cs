﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusExtraBall : BonusEffect {

    public override void OnPickup()
    {
        CommonEngine.StateManager.Instance.GetState<GameplayState>().AddBall();
        DestroyBonus();
    }

}
