﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScore : BonusEffect {

    [SerializeField] int bonusScore;

    public override void OnPickup()
    {
        CommonEngine.StateManager.Instance.GetState<GameplayState>().AddScore(bonusScore);
        DestroyBonus();
    }
}
