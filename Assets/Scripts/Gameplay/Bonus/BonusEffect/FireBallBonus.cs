﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallBonus : BonusEffect
{

    public override void OnPickup()
    {
        foreach(var ball in GameObject.FindObjectsOfType<Ball>())
        {
            if(ball.GetComponent<FireBall>() == null)
                ball.gameObject.AddComponent<FireBall>();
        }
        DestroyBonus();
    }
}
