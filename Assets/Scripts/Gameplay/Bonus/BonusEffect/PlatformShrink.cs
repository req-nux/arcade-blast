﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformShrink : BonusEffect {

    [SerializeField] float expandValue;

    public override void OnPickup()
    {
        var platform = GameObject.FindObjectOfType<Platform>();
        var scale = platform.transform.localScale;
        platform.transform.localScale = new Vector3(Mathf.Max(scale.x + expandValue, 0.25f), scale.y, scale.z);
        DestroyBonus();
    }
}
