﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderBonus : BonusEffect {

    public override void OnPickup()
    {
        foreach(var ball in GameObject.FindObjectsOfType<Ball>())
        {
            if(ball.GetComponent<ThunderBall>() == null)
                ball.gameObject.AddComponent<ThunderBall>();
        }
        DestroyBonus();
    }
}
