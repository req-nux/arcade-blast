﻿using UnityEngine;
using System.Collections;

public class CameraSizeScaling : MonoBehaviour {

    [SerializeField] float xScreenUnitsSize = 5.94f;

    void Awake() {
            GetComponent<Camera> ().orthographicSize = xScreenUnitsSize / (2f*GetComponent<Camera> ().aspect);
    }
}
