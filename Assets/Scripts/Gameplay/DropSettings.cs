﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropSettings : ScriptableObject {

    public float bonusDropChance = 0.1f;
    public List<DropPair> dropList;

    float weightSum;

    void OnEnable()
    {
        weightSum = 0;
        foreach(var pair in dropList)
            weightSum += pair.dropWeight;
    }

    public GameObject GetItem()
    {
        var rand = Random.Range(0f, weightSum);
        var value = 0f;

        for(int i = 0; i < dropList.Count; i++)
        {
            value += dropList[i].dropWeight;
            if(rand < value)
                return dropList[i].bonus;
        }
        return null;
    }
}

[System.Serializable]
public struct DropPair {
    public GameObject bonus;
    public float dropWeight;
}
