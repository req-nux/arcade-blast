﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverArea : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.GetComponent<Ball>() != null)
        {
            //SceneManager.LoadScene("Gameplay");
            StateManager.Instance.GetState<GameplayState>().OnBallDrop();
        }
        else if(col.GetComponent<Block>() != null)
        {
            col.GetComponent<Block>().DestroyBlock();
        }
    }
}
