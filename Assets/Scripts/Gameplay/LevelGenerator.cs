﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelGenerator : MonoBehaviour {

    [SerializeField] GameObject blockPrefab;

    public Vector2 blockPositionShift = new Vector2(0.65f,0.33f);
    public float minHeightToPlaceBlock = 0.4f;
    public float minColorVal = 0.8f;

    GameObject[,] level;
    float[,] levelHeightMap;

    Vector3 colorMultipler;

    int blocksCount = 0;

    public void Run() {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Gameplay"));
        blocksCount = 0;
        var levelSize = GetComponent<LevelSettings>().levelSize;
        level = new GameObject[(int)levelSize.x, (int)levelSize.y];

        colorMultipler = GetColorMultipler();

        while(blocksCount < 10)
        {
            levelHeightMap = GenerateHeightMap((int)levelSize.x, (int)levelSize.y, RandomSeed(Random.value));
            GenerateLevel();
        }
        CommonEngine.StateManager.Instance.GetState<GameplayState>().blocksCount = blocksCount;
    }

    void GenerateLevel () {
        var levelSize = GetComponent<LevelSettings>().levelSize;

        var startPos = new Vector2(-levelSize.x / 2 * blockPositionShift.x + blockPositionShift.x/2,
                                   -levelSize.y / 2 * blockPositionShift.y + levelSize.y / 9);

        for(int i = 0; i <= (int)levelSize.x/2; i++)
        {
            for(int j = 0; j < levelSize.y; j++)
            {
                if(levelHeightMap[i, j] >= minHeightToPlaceBlock)
                {
                    PlaceBlock(startPos, i, j);
                    if(i != (int)levelSize.x / 2)
                        PlaceBlock(startPos, (int)levelSize.x - i - 1, j);
                }
            }
        }
    }

    void PlaceBlock(Vector2 startPos, int i, int j)
    {
        var block = Instantiate(blockPrefab);
        level[i, j] = block;
        blockPrefab.transform.position = new Vector2(startPos.x + i * blockPositionShift.x,
                                                     startPos.y + j * blockPositionShift.y);
        ColorBlock(block.GetComponentInChildren<SpriteRenderer>(), levelHeightMap[i,j]);
        blocksCount++;
    }



    float[,] GenerateHeightMap(int sizeX, int sizeY, NoiseSeed noiseSeed)
    {
        float[,] heightMap = new float[sizeX, sizeY];
        float noiseSampleX;
        float noiseSampleY;

        float shiftDirextionX = noiseSeed.scale;
        float shiftDirextionY = noiseSeed.scale;

        for(float x = 0; x < heightMap.GetLength(0); x++)
        {
            for(float y = 0; y < heightMap.GetLength(1); y++)
            {
                noiseSampleX = noiseSeed.startX + x / heightMap.GetLength(0) * noiseSeed.scale + shiftDirextionX;
                noiseSampleY = noiseSeed.startY + y / heightMap.GetLength(1) * noiseSeed.scale + shiftDirextionY;
                heightMap[(int)x, (int)y] = Mathf.PerlinNoise(noiseSampleX, noiseSampleY);
            }
        }
        return heightMap;
    }

    void ColorBlock(SpriteRenderer sr, float heightMapVal)
    {
        var r = colorMultipler.x * heightMapVal;
        var g = colorMultipler.y * heightMapVal;
        var b = colorMultipler.z * heightMapVal;

        sr.color = new Color(r, g, b);
    }

    Vector3 GetColorMultipler()
    {
        var multipler = Vector3.zero;

        while(multipler.magnitude < 1 || multipler.magnitude > 1.5f)
        {
            multipler = new Vector3(
                     Mathf.RoundToInt(Random.value)
                    ,Mathf.RoundToInt(Random.value)
                    ,Mathf.RoundToInt(Random.value)
               );
        }
        return multipler;
    }

    NoiseSeed RandomSeed(float scale)
    {
        float noiseStartX = Random.value;
        float noiseStartY = Random.value;
        float noiseSampleScale = Random.Range(scale*2, scale * 3.5f);
        return new NoiseSeed(noiseStartX, noiseStartY, noiseSampleScale);
    }

    struct NoiseSeed
    {
        public float startX;
        public float startY;
        public float scale;

        public NoiseSeed(float startX, float startY, float scale)
        {
            this.startX = startX;
            this.startY = startY;
            this.scale = scale;
        }

    }
}
