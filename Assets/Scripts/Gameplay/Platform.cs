﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

    [HideInInspector]
    public Vector3 ballAttachPos;

    GameplayState state;

    void Awake () {
        ballAttachPos = transform.GetComponentInChildren<Ball>().transform.localPosition;
        state = CommonEngine.StateManager.Instance.GetState<GameplayState>();
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<Ball>() != null)
        {
            state.OnPlatformHit();
        }
    }

}
