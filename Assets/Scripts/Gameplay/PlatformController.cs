﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour {

    Ball attachedBall;
    Platform platform;
    StateManager stateManager;
    GameplayState gameplay;

    public int fingerReleaseCount = 0;


    Transform platformTransform;
    Camera gameCamera;
    void Awake () {
        attachedBall = GameObject.FindObjectOfType<Ball>();
        platform = GetComponent<Platform>();
        stateManager = StateManager.Instance;
        gameplay = stateManager.GetState<GameplayState>();

        platformTransform = transform;
        gameCamera = Camera.main;
    }
    
    void Update () {
        if(!(stateManager.GetActiveState() is GameplayState))
            return;

#if UNITY_EDITOR

        if(!Input.GetKey(KeyCode.Mouse0) && fingerReleaseCount > 1)
        {
            gameplay.SetPause(true);
        }
        else
            gameplay.SetPause(false);

        if(Input.GetKeyUp(KeyCode.Mouse0) && attachedBall != null && attachedBall.attached)
            LaunchBall();
        else if(Input.GetKey(KeyCode.Mouse0))
            platformTransform.position = new Vector3(gameCamera.ScreenToWorldPoint(Input.mousePosition).x, platformTransform.position.y, 0);

        if(Input.GetKeyUp(KeyCode.Mouse0))
            fingerReleaseCount++;

#else

        if(Input.touchCount <= 0 && fingerReleaseCount > 1)
        {
            gameplay.SetPause(true);
        }
        else
            gameplay.SetPause(false);

        if(Input.GetTouch(0).phase == TouchPhase.Ended && attachedBall != null && attachedBall.attached)
            LaunchBall();
        else if(Input.GetTouch(0).phase == TouchPhase.Began || Input.GetTouch(0).phase == TouchPhase.Moved)
            platformTransform.position = new Vector3(gameCamera.ScreenToWorldPoint(Input.GetTouch(0).position).x, platformTransform.position.y, 0);
        
        
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
            fingerReleaseCount++;
#endif
    }

    void LaunchBall()
    {
        if(!attachedBall.attached)
            return;

        attachedBall.transform.SetParent(null);
        Vector2 dir = attachedBall.transform.position - platformTransform.position;
        attachedBall.attached = false;
        attachedBall.body.velocity = dir.normalized * attachedBall.speed;

    }

    public void AttachBall(Ball ball)
    {
        attachedBall = ball;
        attachedBall.transform.SetParent(platformTransform);
        attachedBall.attached = true;
        attachedBall.body.velocity = Vector2.zero;
        attachedBall.transform.localPosition = platform.ballAttachPos;
        platformTransform.position.SetX(0);
    }
}
