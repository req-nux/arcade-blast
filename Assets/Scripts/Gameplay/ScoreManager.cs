﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : CommonEngine.Utils.MonoBehaviourSingleton<ScoreManager> {

    [HideInInspector]
    public int tempSessionScore;

    public void SaveScore()
    {
        List<int> scoreList = new List<int>(11);
        for(int i = 0; i < 10; i++)
        {
            scoreList.Add(PlayerPrefs.GetInt(PlayerPrefsKeys.Highscore + i.ToString(), 0));
        }
        scoreList.Add(tempSessionScore);
        scoreList.Sort();
        scoreList.Reverse();
        scoreList.RemoveAt(10);

        for(int i = 0; i < scoreList.Count; i++)
            PlayerPrefs.SetInt(PlayerPrefsKeys.Highscore + i.ToString(), scoreList[i]);
    }
}
