﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefsKeys {

    public const string TotalScore = "totalScore";
    public const string Score = "totalScore";
    public const string Balls = "balls";
    public const string Highscore = "highscore";
}
