﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class ExtraBallState : State {

    public override void Init(StateInitParams initParams)
    {
        CreateView<ExtraBallStateView>();
        ((ExtraBallStateView)view).Init(OnExtraBall, OnLoose);
    }

    void OnExtraBall()
    {
        if(ConnectionManager.Instance.HasInternetConnection())
            AdsManager.Instance.ShowRewardedAd(HandleAdResult);
        else
            StateManager.Instance.PushState<NoConnectionState>(null);
    }

    void OnLoose()
    {
        StateManager.Instance.PopState();
        StateManager.Instance.PushState<GameOverState>(null);
    }

    void HandleAdResult(AdResult result)
    {
        if(result == AdResult.Finished)
        {
            StateManager.Instance.PopState();
            Action action = StateManager.Instance.GetState<GameplayState>().OnExtraBallReceive;
            StateManager.Instance.PushState<RewardState>(new RewardStateInitParams(action));

        }
        else
        {
            Debug.Log("Failed to watch ad");
        }
    }
}
