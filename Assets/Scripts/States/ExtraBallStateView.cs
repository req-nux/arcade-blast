﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraBallStateView : StateView {

    public Button extraBallButton;
    public Button looseButton;

    public void Init(System.Action onExtraBall, System.Action onLoose)
    {
        extraBallButton.onClick.AddListener(() => onExtraBall());
        looseButton.onClick.AddListener(() => onLoose());
    }
}
