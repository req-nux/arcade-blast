﻿using UnityEngine;
using CommonEngine;
using System;


public class GameOverState : State {

    bool scoreSaved;

    public override void Init(StateInitParams initParams)
    {
        CreateView<GameOverStateView>();
        ((GameOverStateView)view).Init(OnExtraScore, OnTryAgain, OnMainMenu);
        scoreSaved = false;
    }

    void OnExtraScore()
    {
        if(ConnectionManager.Instance.HasInternetConnection())
            AdsManager.Instance.ShowRewardedAd(HandleAdResult);
        else
            StateManager.Instance.PushState<NoConnectionState>(null);
    }

    void OnTryAgain()
    {
        if(!scoreSaved)
            ScoreManager.Instance.SaveScore();

        StateManager.Instance.PopStateUntil<GameplayState>();
        StateManager.Instance.GetState<GameplayState>().RestartGameplay();

        /*
        StateManager.Instance.PopState();

        Action onLoadAction = () => StateManager.Instance.PushState<GameplayState>(null);
        var initParams = new SceneLoadInitParams("Gameplay", onLoadAction);
        StateManager.Instance.PushState<SceneLoadState>(initParams);
       */
    }

    void OnMainMenu()
    {
        StateManager.Instance.PopStateUntil<StartScreenState>();
        if(!scoreSaved)
            ScoreManager.Instance.SaveScore();
    }

    void HandleAdResult(AdResult result)
    {
        if(result == AdResult.Finished)
        {
            ScoreManager.Instance.tempSessionScore += Mathf.CeilToInt(ScoreManager.Instance.tempSessionScore / 2f);
            ScoreManager.Instance.SaveScore();
            scoreSaved = true;

            ((GameOverStateView)view).extraScoreButton.gameObject.SetActive(false);
            ((GameOverStateView)view).scoreText.GetComponent<ExtraScoreEffect>().PlayEffect(ScoreManager.Instance.tempSessionScore);
        }
        else
        {
            Debug.Log("Failed to watch ad");
        }
    }

}