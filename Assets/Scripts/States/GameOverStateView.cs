﻿using CommonEngine;
using UnityEngine.UI;

public class GameOverStateView : StateView
{
    public Text scoreText;
    public Button extraScoreButton;
    public Button tryAgainButton;
    public Button mainMenuButton;

    public void Init(System.Action onExtraScore, System.Action onTryAgain, System.Action onMainMenu)
    {
        extraScoreButton.onClick.AddListener(() => onExtraScore());
        tryAgainButton.onClick.AddListener(() => onTryAgain());
        mainMenuButton.onClick.AddListener(() => onMainMenu());
        scoreText.text = ScoreManager.Instance.tempSessionScore.ToString();
    }
}
