﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameplayState : State {

    public int blocksCount;
    public int startHealth = 1;

    public Ball ballPrefab;

    GameSession session;

    int scorePerBlock = 10;

    int blocksToDrop = 3;
    int platformHitsToDrop = 4;
    int platformHitCount = 0;

    LevelGenerator levelGenerator;

    public override void Init(StateInitParams initParams)
    {
        CreateView<GameplayStateView>();

        levelGenerator = GameObject.FindObjectOfType<LevelGenerator>();
        StartGameplay();
        ((GameplayStateView)view).UpdateHealth(session.health);
    }


    public void OnBlockDestroy()
    {
        blocksCount--;
        AddScore(scorePerBlock);
        if(blocksCount == 0)
            OnLevelFinished();
    }
    void OnLevelFinished()
    {
        PlayerPrefs.SetInt(PlayerPrefsKeys.Score, session.score);
        session.startBallSpeedIncrease += GameObject.FindObjectOfType<Ball>().speedGainOnLevel;
        foreach(var bonus in GameObject.FindObjectsOfType<Bonus>())
            Destroy(bonus.gameObject);
        GameObject.FindObjectOfType<PlatformController>().fingerReleaseCount = 0;
        GameObject.FindObjectOfType<PlatformController>().enabled = false;
        StateManager.Instance.PushState<LevelFinishedState>(null);
    }

    public void AddScore(int score)
    {
        session.score += score;
        ((GameplayStateView)view).UpdateScore(session.score);
    }

    public void OnBallDrop()
    {
        session.health--;
        ((GameplayStateView)view).UpdateHealth(session.health);
        if(session.health < 0)
        {
            GameOver();
            session.wasGameOverCalled = true;
        }
        else
        {
            ResetBall();
            DestroyBonuses();
        }
        GameObject.FindObjectOfType<PlatformController>().fingerReleaseCount = 0;
    }

    public void OnPlatformHit()
    {
        if(blocksCount > blocksToDrop)
            return;

        platformHitCount++;
        if(platformHitCount >= platformHitsToDrop)
            StartCoroutine(DropRemainingBlocks());
    }

    public void OnScoreChange()
    {
        ((GameplayStateView)view).UpdateScore(session.score);
    }


    void GameOver()
    {
        GameObject.FindObjectOfType<PlatformController>().enabled = false;
        ScoreManager.Instance.tempSessionScore = session.score;
        if(!session.wasGameOverCalled)
            StateManager.Instance.PushState<ExtraBallState>(null);
        else
        {
            StateManager.Instance.PushState<GameOverState>(null);
        }
    }

    public void StartGameplay()
    {
        foreach(var block in GameObject.FindGameObjectsWithTag("RemoveOnReset"))
            Destroy(block.gameObject);
        blocksCount = 0;

        levelGenerator.Run();
        if(session == null)
        {
            session = new GameSession(startHealth);
            session.health = startHealth;
        }
        else
        {
            ResetBall();
            DestroyBonuses();
        }
        GameObject.FindObjectOfType<PlatformController>().enabled = true;
    }

    public void RestartGameplay()
    {
        foreach(var block in GameObject.FindObjectsOfType<Block>())
            Destroy(block.gameObject);
        blocksCount = 0;
        DestroyBonuses();
        session = new GameSession(startHealth);
        session.health = startHealth;
        ResetBall();
        ((GameplayStateView)view).UpdateHealth(session.health);
        ((GameplayStateView)view).UpdateScore(session.score);

        levelGenerator.Run();
        GameObject.FindObjectOfType<PlatformController>().enabled = true;
    }

    public void SetPause(bool paused)
    {
        ((GameplayStateView)view).pausePanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }
    void ResetBall()
    {
        foreach(var ball in GameObject.FindObjectsOfType<Ball>())
        {
            Destroy(ball.gameObject);
        }
        GameObject.FindObjectOfType<Platform>().transform.localScale = Vector3.one;
        var newBall = Instantiate(ballPrefab);
        GameObject.FindObjectOfType<PlatformController>().AttachBall(newBall);
        platformHitCount = 0;
        if(session != null)
        {
            newBall.speed += session.startBallSpeedIncrease;
        }
    }

    public void OnExtraBallReceive()
    {
        StateManager.Instance.PopStateUntil<GameplayState>();
        AddBall();
        ResetBall();
        DestroyBonuses();
        GameObject.FindObjectOfType<PlatformController>().enabled = true;
    }

    public void OnPlayAgain()
    {
        StateManager.Instance.PopStateUntil<GameplayState>();
        session = null;
        ResetBall();
        DestroyBonuses();
        GameObject.FindObjectOfType<PlatformController>().enabled = true;
        StartGameplay();
    }
    
    public void AddBall()
    {
        session.health++;
        ((GameplayStateView)view).UpdateHealth(session.health);
    }

    public override void Freeze()
    {
        base.Freeze();
        Time.timeScale = 0;
    }
    public override void Unfreeze()
    {
        base.Unfreeze();
        Time.timeScale = 1;
    }
    public override void Finish()
    {
        base.Finish();
        session = null;
        UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync("Gameplay");
    }

    void DestroyBonuses()
    {
        foreach(var bonus in GameObject.FindObjectsOfType<Bonus>())
            Destroy(bonus.gameObject);
    }

    IEnumerator DropRemainingBlocks()
    {
        var interval = 0.15f;
        foreach(var block in GameObject.FindObjectsOfType<Block>())
        {
            yield return new WaitForSeconds(interval);
            block.gameObject.AddComponent<Rigidbody2D>();
        }
    }

}


