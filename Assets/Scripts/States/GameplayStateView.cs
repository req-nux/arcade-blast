﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameplayStateView : StateView
{
    [SerializeField] GameObject healthPrefab;

    public GameObject healthPanel;
    public GameObject pausePanel;
    public Text score;

    public void UpdateHealth(int health)
    {
        var diff = health - healthPanel.transform.childCount;
        if(diff == 0)
            return;

        if(diff > 0)
        {
            for(int i = 0; i < diff; i++)
            {
                Instantiate(healthPrefab, healthPanel.transform, false);
            }
        }
        else
        {
            for(int i = 0; i < Mathf.Abs(diff); i++)
            {
                if(healthPanel.transform.childCount > 0)
                    Destroy(healthPanel.transform.GetChild(0).gameObject);
            }
        }
    }
    public void UpdateScore(int score)
    {
        this.score.text = score.ToString();
    }
}
