﻿using CommonEngine;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreState : State {

    public override void Init(StateInitParams initParams)
    {
        CreateView<HighscoreStateView>();
        ((HighscoreStateView)view).Init(OnReturn);
        FillHighscoreList();
    }

    void FillHighscoreList()
    {
        Text[] list = ((HighscoreStateView)view).scoreList.GetComponentsInChildren<Text>();

        for(int i = 0; i < list.Length; i++)
        {
            var score = PlayerPrefs.GetInt(PlayerPrefsKeys.Highscore + i.ToString(), 0);
            list[i].text = score.ToString();
        }
    }

    void OnReturn()
    {
        StateManager.Instance.PopState();
    }
}
