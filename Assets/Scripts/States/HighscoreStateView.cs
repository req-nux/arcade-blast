﻿using CommonEngine;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreStateView : StateView {

    public Button returnButton;
    public GameObject scoreList; 

    public void Init(System.Action onReturn)
    {
        returnButton.onClick.AddListener(() => onReturn());
    }
}
