﻿using System;
using System.Collections;
using System.Collections.Generic;
using CommonEngine;
using UnityEngine;

public class LevelFinishedState : CommonEngine.State {

    public override void Init(StateInitParams initParams)
    {
        CreateView<LevelFinishedStateView>();
        var score = PlayerPrefs.GetInt(PlayerPrefsKeys.Score, 0);
        ((LevelFinishedStateView)view).Init(OnContinue, score);
    }


    void OnContinue()
    {
        StateManager.Instance.PopState();
        StateManager.Instance.GetState<GameplayState>().StartGameplay();
    }
    
}
