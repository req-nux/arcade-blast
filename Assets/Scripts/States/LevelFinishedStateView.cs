﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelFinishedStateView : CommonEngine.StateView {

    public Button buttonContinue;
    public Text scoreText;

    public void Init(Action onCountinue, int score)
    {
        buttonContinue.onClick.AddListener(() => onCountinue());
        scoreText.text = score.ToString();
    }
    
}
