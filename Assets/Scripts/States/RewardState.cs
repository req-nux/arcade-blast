﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RewardStateInitParams : StateInitParams
{
    public System.Action onOkAction;
    public RewardStateInitParams(System.Action onOk)
    {
        this.onOkAction = onOk;
    }
}

public class RewardState : State {

    public override void Init(StateInitParams initParams)
    {
        CreateView<RewardStateView>();
        ((RewardStateView)view).Init(((RewardStateInitParams)initParams).onOkAction);
    }
}
