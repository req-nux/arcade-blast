﻿using CommonEngine;
using System;
using UnityEngine.UI;

public class RewardStateView : StateView {

    public Button okButton;

    public void Init(Action onOk)
    {
        okButton.onClick.AddListener(() => onOk());
    }
}
