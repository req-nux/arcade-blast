﻿using CommonEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StartScreenState : State {

    public override void Init(StateInitParams initParams)
    {
        CreateView<StartScreenStateView>();
        ((StartScreenStateView)view).Init(OnPlay, OnHighscores, OnSettings, OnCredits);
    }

    void OnPlay()
    {
        Action onLoadAction = () => StateManager.Instance.PushState<GameplayState>(null);
        var initParams = new SceneLoadInitParams("Gameplay", onLoadAction);
        StateManager.Instance.PushState<SceneLoadState>(initParams);
    }

    void OnHighscores()
    {
        StateManager.Instance.PushState<HighscoreState>(null);
    }
    void OnSettings()
    {

    }
    void OnCredits()
    {

    }

}
