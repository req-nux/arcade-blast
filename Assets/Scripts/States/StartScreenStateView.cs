﻿using System;
using CommonEngine;
using UnityEngine.UI;

public class StartScreenStateView : StateView {

    public Button playButton;
    public Button highscoresButton;
    public Button creditsButton;

    public void Init(Action onPlay, Action onShop, Action onSettings, Action onCredits)
    {
        playButton.onClick.AddListener(() => onPlay());
        highscoresButton.onClick.AddListener(() => onShop());
        //creditsButton.onClick.AddListener(() => onCredits());
    }

}
