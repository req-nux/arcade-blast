﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : CommonEngine.Utils.MonoBehaviourSingleton<AudioManager> {

    AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Play(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}
