﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtraScoreEffect : MonoBehaviour {

    [SerializeField] AudioClip clip;
    int newScore;

    public void PlayScoreSwap()
    {
        GetComponent<Text>().text = newScore.ToString();
    }

    public void PlayEffect(int newScore)
    {
        this.newScore = newScore;
        GetComponent<Animator>().updateMode = AnimatorUpdateMode.UnscaledTime;
        GetComponent<Animator>().Play("ExtraScoreAnim");
        AudioManager.Instance.Play(clip);
    }
}
